import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import { MESSAGES_TYPES } from 'constants';
import { Video, Image, Message, Snippet, QuickReply } from 'messagesComponents';
import {
  addAutoUserMessage,
  emitUserMessage,
  displayTypingAfterUserMessage
} from 'actions';

import './styles.scss';

const scrollToBottom = () => {
  const messagesDiv = document.getElementById('messages');
  messagesDiv.scrollTop = messagesDiv.scrollHeight;
};

class Messages extends Component {
  componentDidMount() {
    scrollToBottom();
  }

  componentDidUpdate() {
    scrollToBottom();
  }

  getComponentToRender = (message, index, isLast) => {
    const { params } = this.props;
    const ComponentToRender = (() => {
      switch (message.get('type')) {
        case MESSAGES_TYPES.TEXT: {
          return Message;
        }
        case MESSAGES_TYPES.SNIPPET.LINK: {
          return Snippet;
        }
        case MESSAGES_TYPES.VIDREPLY.VIDEO: {
          return Video;
        }
        case MESSAGES_TYPES.IMGREPLY.IMAGE: {
          return Image;
        }
        case MESSAGES_TYPES.QUICK_REPLY: {
          return QuickReply;
        }
        case MESSAGES_TYPES.CUSTOM_COMPONENT: {
          return (
            message.get('component') ||
            (params &&
              params.customComponents &&
              params.customComponents[message.get('props').template_type])
          );
        }
      }
      return null;
    })();

    if (!ComponentToRender) {
      return null;
    }

    const previousMessage =
      this.props.messages && index > 0 && this.props.messages.get(index - 1);

    if (message.get('type') === 'component') {
      const compToRender = (
        <ComponentToRender
          id={index}
          {...message.get('props')}
          isLast={isLast}
          previousMessage={previousMessage}
          sendAutomaticMessage={this.sendAutomaticMessage}
          displayTypingIndicatorAfterUserMessage={
            this.displayTypingIndicatorAfterUserMessage
          }
        />
      );

      const messageTemplateType = message.get('props').template_type;
      if (['tags', 'link', 'offers'].includes(messageTemplateType)) {
        // Add a response message structure
        const includesTags = ['tags'].includes(messageTemplateType);
        const includesOffers = ['offers'].includes(messageTemplateType);
        return (
          <Message
            extraClassName={
              includesTags ? 'tags' : includesOffers ? 'offers' : undefined
            }
            id={index}
            isLast={isLast}
            previousMessage={previousMessage}
            sender={'response'}
          >
            {compToRender}
          </Message>
        );
      }

      return compToRender;
    }
    return (
      <ComponentToRender
        id={index}
        params={params}
        message={message}
        isLast={isLast}
        previousMessage={previousMessage}
      />
    );
  };

  sendAutomaticMessage = message => {
    if (message) {
      this.props.dispatch(addAutoUserMessage(message));
      this.props.dispatch(emitUserMessage(message));
    }
  };

  displayTypingIndicatorAfterUserMessage = display => {
    this.props.dispatch(displayTypingAfterUserMessage(display));
  };

  render() {
    return (
      <div id="messages" className="messages-container">
        {this.props.messages.map((message, index) => (
          <div className="message" key={index}>
            {this.props.profileAvatar && message.get('showAvatar') && (
              <img
                src={this.props.profileAvatar}
                className="avatar"
                alt="profile"
              />
            )}
            {this.getComponentToRender(
              message,
              index,
              index === this.props.messages.size - 1
            )}
          </div>
        ))}
        {this.props.displayTypingIndication && (
          <div className="message">
            <div className="message-main-container">
              <div className="response typing-signal">
                <div id="wave" className="wave">
                  <span className="wave-dot" />
                  <span className="wave-dot" />
                  <span className="wave-dot" />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

Messages.propTypes = {
  displayTypingIndication: PropTypes.bool,
  messages: ImmutablePropTypes.listOf(ImmutablePropTypes.map),
  profileAvatar: PropTypes.string,
  params: PropTypes.object
};

Message.defaultTypes = {
  displayTypingIndication: false
};

export default connect(store => ({
  messages: store.messages,
  displayTypingIndication: store.behavior.get('messageDelayed')
}))(Messages);
