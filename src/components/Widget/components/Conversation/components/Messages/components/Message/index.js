import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { PROP_TYPES } from 'constants';
import sanitizeHtml from 'sanitize-html';
import './styles.scss';

class Message extends PureComponent {
  parseMessage = message => ({
    __html: sanitizeHtml(message, {
      allowedTags: [
        'a',
        'img',
        'p',
        'h1',
        'h2',
        'i',
        'b',
        'chip',
        'bchip',
        'hint',
        'span',
        'div'
      ],
      allowedAttributes: {
        '*': ['class', 'style', 'href', 'src', 'target']
      },
      transformTags: {
        chip: (tagName, attribs) => ({
          tagName: 'span',
          attribs: {
            class: 'message-chip'
          }
        }),
        bchip: (tagName, attribs) => ({
          tagName: 'span',
          attribs: {
            class: 'message-bchip'
          }
        }),
        hint: (tagName, attribs) => ({
          tagName: 'span',
          attribs: {
            class: 'message-hint'
          }
        })
      }
    })
  });

  parseSimpleMessage = message => ({
    __html: sanitizeHtml(message, {
      allowedTags: ['b', 'u', 'i', 'span', 'div'],
      allowedAttributes: {
        '*': ['class', 'style']
      }
    })
  });

  render() {
    const { docViewer, params } = this.props;
    const sender =
      (this.props.message && this.props.message.get('sender')) ||
      this.props.sender;
    const extraClassName =
      (this.props.message && this.props.message.get('extraClassName')) ||
      this.props.extraClassName;
    const previousMessage =
      (this.props.message && this.props.message.get('previousMessage')) ||
      this.props.previousMessage;
    const lastMessageFromDiffSender =
      previousMessage && sender !== previousMessage.get('sender');
    const text = this.props.message && this.props.message.get('text');
    const textType = this.props.message && this.props.message.get('text_type');
    const isHTMLFormat = textType && textType === 'html';

    const children = this.props.children;
    const UserProfileComponent =
      params && params.customComponents && params.customComponents.userProfile;
    const userProfileData =
      this.props.message && this.props.message.get('userData');

    return (
      <div className="message-main-container">
        <div
          className={clsx(
            sender,
            extraClassName,
            lastMessageFromDiffSender ? 'extra-margin' : null
          )}
        >
          <div className="message-text">
            {sender === 'response' && !children && !isHTMLFormat ? (
              <div
                className="message-simple-message"
                dangerouslySetInnerHTML={this.parseSimpleMessage(text)}
              />
            ) : text ? (
              isHTMLFormat ? (
                <div
                  className="message-html-message"
                  dangerouslySetInnerHTML={this.parseMessage(text)}
                />
              ) : (
                <div
                  className="message-simple-message"
                  dangerouslySetInnerHTML={this.parseSimpleMessage(text)}
                />
              )
            ) : (
              children
            )}
          </div>
        </div>
        {params &&
          params.displayUserProfile &&
          sender === 'response' &&
          UserProfileComponent !== undefined &&
          userProfileData && <UserProfileComponent {...userProfileData} />}
      </div>
    );
  }
}

Message.propTypes = {
  children: PropTypes.node,
  sender: PropTypes.string,
  message: PROP_TYPES.MESSAGE,
  docViewer: PropTypes.bool.isRequired,
  params: PropTypes.object
};

const mapStateToProps = state => ({
  docViewer: state.behavior.get('docViewer')
});

export default connect(mapStateToProps)(Message);
