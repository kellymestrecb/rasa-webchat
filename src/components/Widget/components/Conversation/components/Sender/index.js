import React from 'react';
import PropTypes from 'prop-types';

import send from 'assets/send_button.svg';
import './style.scss';

const Sender = ({ senderProps, sendMessage, disabledInput }) => (
  <div className="sender-container">
    {senderProps && senderProps.restartButton}
    <form
      className="sender"
      onSubmit={event => {
        sendMessage(event);
        senderProps &&
          senderProps.onSendMessage &&
          senderProps.onSendMessage(event);
      }}
    >
      <input
        type="text"
        className="new-message"
        name="message"
        placeholder={senderProps && senderProps.inputFieldTextHint}
        disabled={disabledInput || (senderProps && senderProps.disabled)}
        autoFocus={senderProps && senderProps.autoFocus}
        autoComplete="off"
        onFocus={senderProps && senderProps.onFocus}
      />
      <button key={'sender-button'} type="submit" className="send">
        {(senderProps && senderProps.icon) || (
          <img src={send} className="send-icon" alt="send" />
        )}
      </button>
    </form>
  </div>
);

Sender.propTypes = {
  sendMessage: PropTypes.func,
  disabledInput: PropTypes.bool,
  senderProps: PropTypes.shape({
    disabled: PropTypes.bool,
    inputFieldTextHint: PropTypes.string,
    icon: PropTypes.node
  })
};

export default Sender;
