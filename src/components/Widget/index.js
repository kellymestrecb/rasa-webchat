/* eslint-disable no-undef */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  toggleChat,
  openChat,
  closeChat,
  showChat,
  addUserMessage,
  emitUserMessage,
  addResponseMessage,
  addLinkSnippet,
  addVideoSnippet,
  addImageSnippet,
  addQuickReply,
  initialize,
  connectServer,
  disconnectServer,
  pullSession,
  renderCustomComponent,
  triggerMessageDelayed
} from 'actions';

import {
  isSnippet,
  isVideo,
  isImage,
  isQR,
  isText,
  isCustom
} from './msgProcessor';
import WidgetLayout from './layout';
import {
  storeLocalSession,
  getLocalSession
} from '../../store/reducers/helper';
import { SESSION_NAME, NEXT_MESSAGE } from 'constants';

class Widget extends Component {
  constructor(props) {
    super(props);
    this.messages = [];
    this.onGoingMessageDelay = [];

    setInterval(() => {
      if (this.messages.length > 0) {
        this.dispatchMessage(this.messages.shift());
      }
    }, this.props.interval);
  }

  componentDidMount() {
    const { customData, socket, storage } = this.props;

    socket.on('bot_uttered', (botUttered) => {
      this.handleMessageReceived(botUttered);
    });

    this.props.dispatch(pullSession());

    // Request a session from server
    const local_id = this.getSessionId();
    socket.on('connect', () => {
      socket.emit('session_request', {
        session_id: local_id,
        customData
      });
    });

    // When session_confirm is received from the server:
    socket.on('session_confirm', (remote_id) => {
      // Store the initial state to both the redux store and the storage, set connected to true
      this.props.dispatch(connectServer());

      /*
      Check if the session_id is consistent with the server
      If the local_id is null or different from the remote_id,
      start a new session.
      */
      if (local_id !== remote_id) {
        // storage.clear();
        // Store the received session_id to storage

        storeLocalSession(storage, SESSION_NAME, remote_id);
        this.props.dispatch(pullSession());
        this.trySendInitPayload();
      } else {
        // If this is an existing session, it's possible we changed pages and want to send a
        // user message when we land.
        const nextMessage = window.localStorage.getItem(NEXT_MESSAGE);

        if (nextMessage !== null) {
          const { message, expiry } = JSON.parse(nextMessage);
          window.localStorage.removeItem(NEXT_MESSAGE);

          if (expiry === 0 || expiry > Date.now()) {
            const { params } = this.props;
            this.props.dispatch(addUserMessage(message, params));
            this.props.dispatch(emitUserMessage(message, params));
          }
        }
      }
    });

    this.unloadEventListener = () => {
      socket.emit('custom_close', customData);
    };

    window.addEventListener('unload', this.unloadEventListener);

    socket.on('disconnect', (reason) => {
      console.log(reason);
      if (reason !== 'io client disconnect') {
        this.props.dispatch(disconnectServer());
      }
    });

    if (this.props.chatOpened) {
      this.props.dispatch(showChat());
      this.props.dispatch(openChat());
    } else {
      if (this.props.embedded && this.props.initialized) {
        this.props.dispatch(showChat());
        this.props.dispatch(openChat());
      }
      if (!this.props.embedded) {
        this.props.dispatch(closeChat());
      }
    }
  }

  handleMessageReceived(botUttered) {
    const { dispatch } = this.props;

    this.onGoingMessageDelay.push(true);
    dispatch(triggerMessageDelayed(true));
    this.newMessageTimeout(botUttered);
  }

  newMessageTimeout(message) {
    const { dispatch } = this.props;

    setTimeout(() => {
      this.dispatchMessage(message);

      this.onGoingMessageDelay.pop();
      this.onGoingMessageDelay.length === 0 &&
        dispatch(triggerMessageDelayed(false));
    }, this.onGoingMessageDelay.length * 1000);
  }

  componentDidUpdate(prevProps) {
    const { customData, socket, storage } = this.props;
    this.props.dispatch(pullSession());
    this.trySendInitPayload();

    if (
      this.props.chatOpened &&
      !prevProps.initialized &&
      this.props.initialized
    ) {
      this.props.dispatch(showChat());
      this.props.dispatch(openChat());
    } else {
      if (this.props.embedded && this.props.initialized) {
        this.props.dispatch(showChat());
        this.props.dispatch(openChat());
      }
      const needToCloseChat =
        !prevProps.initialized &&
        this.props.initialized &&
        !this.props.embedded;
      if (needToCloseChat) {
        this.props.dispatch(closeChat());
      }
    }

    const local_id = this.getSessionId();
    const needSessionRequest = !local_id;
    if (needSessionRequest) {
      // Request a session from server
      socket.on('connect', () => {
        socket.emit('session_request', {
          session_id: local_id,
          customData
        });
      });

      // When session_confirm is received from the server:
      socket.on('session_confirm', (remote_id) => {
        // Store the initial state to both the redux store and the storage, set connected to true
        this.props.dispatch(connectServer());

        // Store the received session_id to storage

        storeLocalSession(storage, SESSION_NAME, remote_id);
        this.props.dispatch(pullSession());
        this.trySendInitPayload();
      });
    }
  }

  componentWillUnmount() {
    const { socket } = this.props;
    socket.close();

    window.removeEventListener('unload', this.unloadEventListener);
  }

  getSessionId() {
    const { storage } = this.props;
    // Get the local session, check if there is an existing session_id
    const localSession = getLocalSession(storage, SESSION_NAME);
    const local_id = localSession ? localSession.session_id : null;
    return local_id;
  }

  // TODO: Need to erase redux store on load if localStorage
  // is erased. Then behavior on reload can be consistent with
  // behavior on first load

  trySendInitPayload = () => {
    const {
      initPayload,
      customData,
      socket,
      initialized,
      isChatOpen,
      isChatVisible,
      embedded,
      connected
    } = this.props;

    // Send initial payload when chat is opened or widget is shown
    if (
      !initialized &&
      connected &&
      ((isChatOpen && isChatVisible) || embedded)
    ) {
      // Only send initial payload if the widget is connected to the server but not yet initialized

      const session_id = this.getSessionId();

      // check that session_id is confirmed
      if (!session_id) return;
      console.log('sending init payload', session_id);
      socket.emit('user_uttered', {
        message: initPayload,
        customData,
        session_id
      });
      this.props.dispatch(initialize());
    }
  };

  toggleConversation = () => {
    this.props.dispatch(toggleChat());
  };

  dispatchMessage(message) {
    if (Object.keys(message).length === 0) {
      return;
    }
    if (isText(message)) {
      this.props.dispatch(addResponseMessage(message.text, message.params));
    } else if (isQR(message)) {
      this.props.dispatch(addQuickReply(message));
    } else if (isSnippet(message)) {
      const element = message.attachment.payload.elements[0];
      this.props.dispatch(
        addLinkSnippet({
          title: element.title,
          content: element.buttons[0].title,
          link: element.buttons[0].url,
          target: '_blank'
        })
      );
    } else if (isVideo(message)) {
      const element = message.attachment.payload;
      this.props.dispatch(
        addVideoSnippet({
          title: element.title,
          video: element.src
        })
      );
    } else if (isImage(message)) {
      const element = message.attachment.payload;
      this.props.dispatch(
        addImageSnippet({
          title: element.title,
          image: element.src
        })
      );
    } else if (isCustom(message)) {
      const { params } = this.props;
      const payload = message.attachment.payload;
      const component = params.customComponents[payload.template_type];
      this.props.dispatch(renderCustomComponent(component, payload, params));
    }
  }

  handleMessageSubmit = (event) => {
    event.preventDefault();
    const userUttered = event.target.message.value;
    if (userUttered) {
      const { params } = this.props;

      this.props.displayTypingAfterUserMessage &&
        setTimeout(() => {
          this.props.dispatch(triggerMessageDelayed(true));

          setTimeout(() => {
            this.onGoingMessageDelay.length === 0 &&
              this.props.displayTypingIndication &&
              this.props.dispatch(triggerMessageDelayed(false));
          }, 20000);
        }, 1000);

      this.props.dispatch(addUserMessage(userUttered, params));
      this.props.dispatch(emitUserMessage(userUttered, params));
    }
    event.target.message.value = '';
  };

  render() {
    return (
      <div className="widget-main-container" style={this.props.containerStyle}>
        {this.props.isChatOpen && !this.props.embedded && (
          <div
            className="widget-overlay"
            onClick={() => {
              this.props.dispatch(closeChat());
            }}
          />
        )}
        <WidgetLayout
          mainClassName={this.props.mainClassName}
          toggleChat={this.toggleConversation}
          onSendMessage={this.handleMessageSubmit}
          title={this.props.title}
          subtitle={this.props.subtitle}
          header={this.props.header}
          customData={this.props.customData}
          profileAvatar={this.props.profileAvatar}
          showCloseButton={this.props.showCloseButton}
          hideWhenNotConnected={this.props.hideWhenNotConnected}
          fullScreenMode={this.props.fullScreenMode}
          isChatOpen={this.props.isChatOpen}
          isChatVisible={this.props.isChatVisible}
          badge={this.props.badge}
          embedded={this.props.embedded}
          params={this.props.params}
          openLauncherImage={this.props.openLauncherImage}
          closeImage={this.props.closeImage}
          senderProps={this.props.senderProps}
          buttonAction={this.props.buttonAction}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  initialized: state.behavior.get('initialized'),
  connected: state.behavior.get('connected'),
  isChatOpen: state.behavior.get('isChatOpen'),
  isChatVisible: state.behavior.get('isChatVisible'),
  displayTypingIndication: state.behavior.get('messageDelayed'),
  displayTypingAfterUserMessage: state.behavior.get(
    'displayTypingAfterUserMessage'
  )
});

Widget.propTypes = {
  mainClassName: PropTypes.string,
  interval: PropTypes.number,
  title: PropTypes.string,
  chatOpened: PropTypes.bool,
  customData: PropTypes.shape({}),
  subtitle: PropTypes.string,
  header: PropTypes.node,
  initPayload: PropTypes.string,
  profileAvatar: PropTypes.string,
  showCloseButton: PropTypes.bool,
  hideWhenNotConnected: PropTypes.bool,
  fullScreenMode: PropTypes.bool,
  isChatVisible: PropTypes.bool,
  isChatOpen: PropTypes.bool,
  badge: PropTypes.number,
  socket: PropTypes.shape({}),
  embedded: PropTypes.bool,
  params: PropTypes.object,
  connected: PropTypes.bool,
  initialized: PropTypes.bool,
  openLauncherImage: PropTypes.node,
  closeImage: PropTypes.node,
  senderProps: PropTypes.shape({
    disabled: PropTypes.bool,
    inputFieldTextHint: PropTypes.string,
    icon: PropTypes.node,
    restartButton: PropTypes.node
  }),
  buttonAction: PropTypes.node
};

Widget.defaultProps = {
  isChatOpen: false,
  isChatVisible: true
};

export default connect(mapStateToProps)(Widget);
