import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Conversation from './components/Conversation';
import Launcher from './components/Launcher';
import './style.scss';
import { emitNotificationMessage, renderCustomComponent } from 'actions';

const WidgetLayout = (props) => {
  const classes = props.embedded ? ['widget-embedded'] : ['widget-container'];
  if (props.fullScreenMode) {
    classes.push('full-screen');
  }
  const showCloseButton =
    props.showCloseButton !== undefined ? props.showCloseButton : !props.embedded;
  const isVisible = props.isChatVisible && !(props.hideWhenNotConnected && !props.connected);
  const chatShowing = props.isChatOpen || props.embedded;
  const triggerMessage = (message, roomId) => {
    if (message) {
      const templateType = 'notification';
      const component = props.params.customComponents[templateType];
      props.dispatch(
        renderCustomComponent(component, { template_type: templateType, text: message, roomId })
      );
      props.dispatch(
        emitNotificationMessage({ template_type: templateType, text: message, roomId })
      );
    }
  };

  if (chatShowing && !props.embedded) {
    classes.push('chat-open');
  }

  props && props.mainClassName && classes.push(props.mainClassName);

  return isVisible ? (
    <div className={classes.join(' ')}>
      {chatShowing && (
        <Conversation
          title={props.title}
          subtitle={props.subtitle}
          header={props.header}
          sendMessage={props.onSendMessage}
          profileAvatar={props.profileAvatar}
          toggleChat={props.toggleChat}
          isChatOpen={props.isChatOpen}
          disabledInput={props.disabledInput}
          params={props.params}
          {...{ showCloseButton }}
          connected={props.connected}
          connectingText={props.connectingText}
          closeImage={props.closeImage}
          senderProps={props.senderProps}
        />
      )}
      {!props.embedded && (
        <Launcher
          toggle={props.toggleChat}
          isChatOpen={props.isChatOpen}
          badge={props.badge}
          fullScreenMode={props.fullScreenMode}
          openLauncherImage={props.openLauncherImage}
          closeImage={props.closeImage}
        />
      )}
      {props.buttonAction && (
        <div className="custom-button-action">
          {<props.buttonAction triggerMessage={triggerMessage} />}
        </div>
      )}
    </div>
  ) : null;
};

const mapStateToProps = state => ({
  isChatVisible: state.behavior.get('isChatVisible'),
  isChatOpen: state.behavior.get('isChatOpen'),
  disabledInput: state.behavior.get('disabledInput'),
  connected: state.behavior.get('connected'),
  connectingText: state.behavior.get('connectingText')
});

WidgetLayout.propTypes = {
  mainClassName: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  header: PropTypes.node,
  onSendMessage: PropTypes.func,
  toggleChat: PropTypes.func,
  isChatOpen: PropTypes.bool,
  isChatVisible: PropTypes.bool,
  profileAvatar: PropTypes.string,
  showCloseButton: PropTypes.bool,
  hideWhenNotConnected: PropTypes.bool,
  disabledInput: PropTypes.bool,
  fullScreenMode: PropTypes.bool,
  badge: PropTypes.number,
  embedded: PropTypes.bool,
  params: PropTypes.object,
  connected: PropTypes.bool,
  connectingText: PropTypes.string,
  openLauncherImage: PropTypes.node,
  closeImage: PropTypes.node,
  senderProps: PropTypes.shape({
    disabled: PropTypes.bool,
    inputFieldTextHint: PropTypes.string,
    icon: PropTypes.node,
    restartButton: PropTypes.node
  }),
  buttonAction: PropTypes.node
};

export default connect(mapStateToProps)(WidgetLayout);
